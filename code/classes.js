var Car1 = (function () {
    function Car1(horsePower, make, model) {
        this.horsePower = horsePower;
        this.make = make;
        this.model = model;
        console.log("Car1 has been created");
    }
    Car1.prototype.getHorsePower = function () {
        return this.horsePower;
    };
    Car1.prototype.getMake = function () {
        return this.make;
    };
    Car1.prototype.getModel = function () {
        return this.model;
    };
    return Car1;
})();
console.log("Car class Take 1");
var car1 = new Car1(100, "Toyota", "Camry");
console.log("Car's power level (HP)" + car1.getHorsePower());
console.log("Car's make " + car1.getMake());
console.log("Car's model " + car1.getModel());
var Car2 = (function () {
    function Car2(horsePower, make, model) {
        this.horsePower = horsePower;
        this.make = make;
        this.model = model;
        console.log("Car2 has been created");
    }
    Car2.prototype.getHorsePower = function () {
        return this.horsePower;
    };
    Car2.prototype.getMake = function () {
        return this.make;
    };
    Car2.prototype.getModel = function () {
        return this.model;
    };
    return Car2;
})();
console.log("Car class Take 2");
var car2 = new Car2(100, "Toyota", "Camry");
console.log("Car's power level (HP)" + car2.getHorsePower());
console.log("Car's make " + car2.getMake());
console.log("Car's model " + car2.getModel());
